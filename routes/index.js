const express = require("express");
const { Resume, validate } = require("../models/resume");
const mongoose = require("mongoose");
const router = express.Router();
const { ensureAuthenticated } = require("../config/auth");

//Welcome page
router.get("/", (req, res) => {
  res.render("welcome");
});

//Dashboard page
router.get("/dashboard", ensureAuthenticated, (req, res) => {
  res.render("dashboard", {
    name: req.user.name
  });
});

//About Us page
router.get("/about", (req, res) => {
  res.render("about");
});

//Resume page
router.get("/resume", ensureAuthenticated, (req, res) => {
  res.render("resume", { name: req.user.name });
});

//Resume page handle
router.post("/resume", ensureAuthenticated, async (req, res, next) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let resume = new Resume({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    age: req.body.age,
    phone: req.body.phone,
    email: req.body.email,
    address: req.body.address,
    objective: req.body.objective,
    workExperience: req.body.workExperience,
    education: req.body.education,
    skills: req.body.skills
  });
  resume = await resume.save();

  Resume.find(function(err, data) {
    if (!err) {
      res.render("newresume", { records: data });
    } else {
      console.log("Error finding values");
    }
  });
});

//New Resume page
router.get("/newresume", ensureAuthenticated, function(req, res, next) {
  Resume.find(function(err, data) {
    if (!err) {
      res.render("newresume", {
        records: data
      });
    } else {
      console.log("Error finding values");
    }
  });
});
module.exports = router;
