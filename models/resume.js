const Joi = require("joi");
const mongoose = require("mongoose");

const Resume = mongoose.model(
  "Resume",
  new mongoose.Schema({
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    age: {
      type: Number
    },
    email: {
      type: String
    },
    address: {
      type: String
    },
    phone: {
      type: Number
    },
    objective: {
      type: String
    },
    workExperience: {
      type: String
    },
    education: {
      type: String
    },
    skills: {
      type: String
    }
  })
);

function validateResume(resume) {
  const schema = {
    firstName: Joi.string(),
    lastName: Joi.string(),
    age: Joi.number(),
    email: Joi.string(),
    address: Joi.string(),
    phone: Joi.number(),
    objective: Joi.string(),
    workExperience: Joi.string(),
    education: Joi.string(),
    skills: Joi.string()
  };
  return Joi.validate(resume, schema);
}

exports.Resume = Resume;
exports.validate = validateResume;
