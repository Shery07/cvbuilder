const express = require("express");
const bodyParser = require("body-parser");
const expressLayouts = require("express-ejs-layouts");
const mongoose = require("mongoose");
const passport = require("passport");
const flash = require("connect-flash");
const session = require("express-session");
const dbConfig = require("./config/keys.js");
const path = require("path");
const ejs = require("ejs");
//initializing app variable with express
const app = express();
//To add custom css
app.use(express.static("public"));
// Passport Config
require("./config/passport")(passport);

// MongoDB config
const db = require("./config/keys").MongoURI;

// Connect to mongodb
//mongoose
//.connect(db, {
//useUnifiedTopology: true,
//useNewUrlParser: true
//})
//.then(() => console.log("MongoDB connected"))
//.catch(err => console.log(err));

// Connecting to the database
mongoose
  .connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connected to the database");
  })
  .catch(err => {
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
  });

//ejs
app.use(expressLayouts);
app.set("view engine", "ejs");

// Bodyparser
app.use(bodyParser.urlencoded({ extended: false }));

//Express Session
app.use(
  session({
    secret: "secret",
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

//Connect flash
app.use(flash());

//Global Variable
app.use((req, res, next) => {
  res.locals.success_msg = req.flash("success_msg");
  res.locals.error_msg = req.flash("error_msg");
  res.locals.error_msg = req.flash("error");
  next();
});

//Routes
app.use("/", require("./routes/index"));
app.use("/users", require("./routes/users"));

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log(`Server started on port ${PORT}`));
